const express = require('express');
const axios = require('axios');
const helmet = require('helmet');
const qs = require('querystring');
const app = express();

const knownVersions = ['1.0', '1.1'];
// const knownParameters = ['version', 'access_token', 'auth', 'device_id', 'device_type', 'user_id'];
const accessTokens = ['WveH9VkPLrXvuNm'];
var version = { major: 1, minor: 0, string: '1.0' };
var port = process.env.PORT || 3001; // eslint-disable-line

// ==== ROUTING ==== //
// Configure the server
app.enable('trust proxy');
app.disable('view cache');
app.use(helmet());
app.use(helmet.noCache());
// /start_session route
app.get('/start_session', (req, res) => {
	// Check the version for validity
	if (checkVersion(res, req.query)) {
		// Check versionspecific functions
		if (version.major === 1) {
			if (version.minor <= 0 && req.query.auth) {
				// Version <= 1.0: only start_session without logging in is supported
				sendResult(res, true, 'Authentication with auth tokens is disabled in versions lower than 1.1.');
				return;
			} else if (version.minor >= 1 && req.query.auth && !req.query.user_id) {
				// Version >= 1.1: logging in with auth token requires user_id to match
				sendResult(res, true, 'Authentication with auth tokens requires the user_id parameter.');
				return;
			}
		}
		// Prepare the parameters for the request
		let parameters = { ...req.query,
			version: '457',
			access_token: accessTokens[Math.floor(Math.random() * accessTokens.length)], //eslint-disable-line
			device_type: 'com.crunchyroll.crunchyroid' }; //eslint-disable-line
		// Generate a new device_id if none is provided
		if (parameters.device_id === '' || parameters.device_id === undefined || parameters.device_id === null) {
			parameters.device_id = generateId();
		}
		// Send the request to Crunchyroll's server
		sendRequest(res, `https://api.crunchyroll.com/start_session.${version.major}.json`, parameters);
	}
});

// Catch-all route
app.get('/*', (req, res) => {
	// Send error
	sendResult(res, true, 'Invalid API endpoint.');
});

// Start the server at the specified port
app.listen(port, () => {
	console.log('Starting CR-Unblocker-Server endpoint');
	console.log(`Listening on port ${port}`);
});

/**
 * Checks the provided version for validity.
 * @param {Response} query Query from the url
 */
function checkVersion(res, query) {
	// Check if an version was submitted.
	if (query.version) {
		// Parse version into object containing minor and major version
		var splittedVersion = query.version.split('.');
		version = { major: parseInt(splittedVersion[0]) || 0, minor: parseInt(splittedVersion[1]) || 0, string: query.version };
	}
	// Validate version against whitelist
	if (knownVersions.indexOf(version.string) === -1) {
		sendResult(res, true, `Invalid API version specified: ${version.string}`);
		return false;
	}
	return true;
}

/**
 * Sends an request to Crunchyroll's server.
 * @param {String} url
 * @param {String} parameter
 */
function sendRequest(res, url, parameter) {
	// Send the request and parse the result
	axios(`${url}?${qs.stringify(parameter)}`).then((result) => {
		sendResult(res, false, result);
	}).catch((e) => {
		sendResult(res, true, e, parameter);
	});
}

/**
 * Send the result to the user.
 * @param {Object} res  Result object
 * @param {Object} data Object containing the requested payload
 */
function sendResult(res, err, data, parameter) {
	// Check if the result is an error
	if (err === true || data.data.error) {
		// Log the error
		console.log('Something went wrong with the request:');
		if (parameter !== undefined && parameter.access_token !== undefined) {
			console.log(`Used access_token: ${parameter.access_token}`);
		}
		console.log(JSON.stringify(data));
		// Send an error
		res.status(500).send({
			message: data,
			code: 'error',
			error: true
		});
	} else {
		// Format the data object
		data = data.data;
		// If auth is specified, require that user_id matches
		if (data.user && parameter.auth && data.user.user_id !== parameter.user_id) {
			// sendResult(res, true, 'Invalid user ID');
			// return;
		}
		// Send the result
		res.status(200).send(data);
	}
}

/**
 * Generate a random 32 character long device ID.
 * @return {String} Generated device ID
 */
function generateId() {
	let id = '';
	const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (var i = 0; i < 32; i++) {
		id += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return id;
}

module.exports = app;
